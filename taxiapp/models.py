# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
class Taxi(models.Model):
    name = models.CharField(max_length=100)
    phone = models.CharField(max_length=20, null=True, blank=True)
    open_hour = models.TimeField()
    close_hour = models.TimeField()
    logo = models.ImageField(upload_to='uploads/')
    stars = models.IntegerField(default=0)

    def __str__(self):
        return self.name
