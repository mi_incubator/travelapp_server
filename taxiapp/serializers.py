from rest_framework import serializers

from taxiapp.models import Taxi


class TaxiSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Taxi
        fields = ('name', 'phone', 'open_hour', 'close_hour', 'logo', 'stars')
