# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from taxiapp.models import Taxi

admin.site.register(Taxi)
