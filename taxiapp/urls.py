from django.conf.urls import url

from . import views

app_name = 'taxiapp'
urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^(?P<taxi_id>[0-9]+)$', views.edit, name='edit'),
    url(r'^(?P<taxi_id>[0-9]+)/update$', views.update, name='update'),
    url(r'^create/', views.create, name='create'),
    url(r'^store/', views.store, name='store'),
]
