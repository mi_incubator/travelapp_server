# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import Http404
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.views import generic
from rest_framework import viewsets

from taxiapp.models import Taxi
from taxiapp.serializers import TaxiSerializer


class TaxiViewSet(viewsets.ModelViewSet):
    queryset = Taxi.objects.all().order_by('-stars')
    serializer_class = TaxiSerializer


def create(request):
    return render(request, 'taxiapp/create.html')


def store(request):
    # print request.POST
    taxi = Taxi()
    taxi.name = request.POST['name']
    taxi.phone = request.POST['phone']
    taxi.open_hour = request.POST['open_hour']
    taxi.close_hour = request.POST['close_hour']
    taxi.save()

    return HttpResponseRedirect(reverse('taxiapp:index'))


def edit(request, taxi_id):
    # Method 1
    # try:
    #     taxi = Taxi.objects.get(pk=taxi_id)
    # except Taxi.DoesNotExist:
    #     raise Http404("Taxi does not exist")

    # Method 2
    taxi = get_object_or_404(Taxi, pk=taxi_id)

    return render(request, 'taxiapp/edit.html', {'taxi': taxi})


def update(request, taxi_id):
    try:
        taxi = Taxi.objects.get(pk=taxi_id)
    except Taxi.DoesNotExist:
        raise Http404("Taxi does not exist")

    taxi.name = request.POST['name']
    taxi.phone = request.POST['phone']
    taxi.open_hour = request.POST['open_hour']
    taxi.close_hour = request.POST['close_hour']
    taxi.save()

    return HttpResponseRedirect(reverse('taxiapp:index'))


class IndexView(generic.ListView):
    template_name = 'taxiapp/index.html'
    context_object_name = 'taxis'

    def get_queryset(self):
        return Taxi.objects.order_by('-stars')
